# Generated by Django 4.2.11 on 2024-05-02 06:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("pengguna", "0003_biodata_foto"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="biodata", options={"verbose_name_plural": "1. Biodata"},
        ),
    ]
